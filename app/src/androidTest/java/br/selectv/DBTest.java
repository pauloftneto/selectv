package br.selectv;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.test.AndroidTestCase;

import br.selectv.util.DatabaseHelper;


public class DBTest extends AndroidTestCase {


    // metodo que testa a criação do banco de dados
    public void testCreateDB() {
        DatabaseHelper dbHelper = new DatabaseHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        assertTrue(db.isOpen());
        db.close();
    }

    public void testDropDB() {
        assertTrue(mContext.deleteDatabase(DatabaseHelper.BANCO_DADOS));
    }

    // metodo que testa a inserção no dados no banco
    public void testInsertBD() {
        DatabaseHelper dbHelper = new DatabaseHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String objetoJson = "objetoJsonTeste";

        ContentValues contentValues = new ContentValues();
        contentValues.put(DBContract.ObjetoJson.COLMN_JSON, objetoJson);
        int id = (int) db.insert(DBContract.ObjetoJson.DB_NAME, null, contentValues);
        assertTrue(id != 0);
    }

    // metodo que testa a recuperação de dados no banco
    public void testRecDB() {
        DatabaseHelper dbHelper = new DatabaseHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from usuario", null);

        // ao passar para proxima coluna retorna false
        assertEquals(false, cursor.moveToFirst());
    }

    // classe usada para definir nome do banco e as colunas da tabela
    public class DBContract {
        public final class ObjetoJson implements BaseColumns {
            public static final String DB_NAME = "SelectV";
            public static final String COLMN_JSON = "objetoJson";
        }
    }

}
