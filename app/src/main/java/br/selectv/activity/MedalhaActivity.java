package br.selectv.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import br.selectv.R;
import br.selectv.util.AsyncConect;
import br.selectv.util.ConnectionAPI;

public class MedalhaActivity extends AppCompatActivity implements AsyncConect {

    private MedalhaActivity contexto = MedalhaActivity.this;

    ImageView CadPonto;
    ImageView CadPontoSombra;
    ImageView CheckinPonto;
    ImageView CheckinPontoSombra;
    ImageView ComentPonto;
    ImageView ComentPontoSombra;

    TextView tx_medalhaColeta;
    TextView tx_medalhaComent;
    TextView tx_medalhaCheck;

    private final Context mContext = this;

    ProgressDialog pD;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medalha);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        CadPonto = (ImageView) findViewById(R.id.medCadPonto);
        CadPontoSombra = (ImageView) findViewById(R.id.medCadPontoSombra);
        tx_medalhaColeta = (TextView) findViewById(R.id.tx_medalhaColeta);
        tx_medalhaColeta.setVisibility(View.GONE);

        CheckinPonto = (ImageView) findViewById(R.id.medCheckinPonto);
        CheckinPontoSombra = (ImageView) findViewById(R.id.medCheckinPontoSombra);
        tx_medalhaCheck = (TextView) findViewById(R.id.tx_medalhaCheck);
        tx_medalhaCheck.setVisibility(View.GONE);

        ComentPonto = (ImageView) findViewById(R.id.medComentPonto);
        ComentPontoSombra = (ImageView) findViewById(R.id.medComentPontoSombra);
        tx_medalhaComent = (TextView) findViewById(R.id.tx_medalhaComent);
        tx_medalhaComent.setVisibility(View.GONE);


        String pegaarquivo = new MapsActivity().getTOKEN_MEDALHA();
        sharedPreferences = this.getSharedPreferences(pegaarquivo, 0);

        if (sharedPreferences.contains("ponto_coleta")) {
            CadPonto.setVisibility(View.VISIBLE);
            tx_medalhaColeta.setVisibility(View.VISIBLE);
            CadPontoSombra.setVisibility(View.GONE);
        } else {
            CadPonto.setVisibility(View.GONE);
            tx_medalhaColeta.setVisibility(View.GONE);
            CadPontoSombra.setVisibility(View.VISIBLE);
        }

        if (sharedPreferences.contains("check")) {
            CheckinPonto.setVisibility(View.VISIBLE);
            tx_medalhaCheck.setVisibility(View.VISIBLE);
            CheckinPontoSombra.setVisibility(View.GONE);
        } else {
            CheckinPonto.setVisibility(View.GONE);
            tx_medalhaCheck.setVisibility(View.GONE);
            CheckinPontoSombra.setVisibility(View.VISIBLE);
        }

        if (sharedPreferences.contains("comentario")) {
            ComentPonto.setVisibility(View.VISIBLE);
            tx_medalhaComent.setVisibility(View.VISIBLE);
            ComentPontoSombra.setVisibility(View.GONE);
        } else {
            ComentPonto.setVisibility(View.GONE);
            tx_medalhaComent.setVisibility(View.GONE);
            ComentPontoSombra.setVisibility(View.VISIBLE);
        }

        LeituraMedalha();

    }

    private void LeituraMedalha() {
        String pegaarquivo = new LoginActivity().getDADOS_USUARIO();
        sharedPreferences = this.getSharedPreferences(pegaarquivo, 0);

        HashMap<String, String> data = new HashMap<>();

        data.put("url", "http://paulo.winserverpro.com.br/API/Usuario.php");
        data.put("acao", "Leitura");
        data.put("id", sharedPreferences.getString("id", ""));

        ConnectionAPI conexao = new ConnectionAPI();
        conexao.delegate = contexto;
        conexao.execute(data);

        pD = new ProgressDialog(mContext);
        pD.setMessage("Carregando informações");
        pD.setIndeterminate(false);
        pD.setCancelable(false);
        pD.show();
    }

    @Override
    public void processFinish(JSONObject resposta) {
        pD.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
    }
}
