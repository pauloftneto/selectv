package br.selectv.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import br.selectv.R;
import br.selectv.model.Usuario;
import br.selectv.util.AsyncConect;
import br.selectv.util.BancoController;
import br.selectv.util.ConnectionAPI;

public class LoginActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        View.OnClickListener, AsyncConect {

    CallbackManager callbackManager;

    private final String DADOS_USUARIO = "dados_usuario";

    public String getDADOS_USUARIO() {
        return DADOS_USUARIO;
    }

    SharedPreferences sharedPreferences;

    private static final String TAG = "activity_login";
    private static final int RC_SIGN_IN = 9001;

    private LoginActivity contexto = LoginActivity.this;
    private GoogleApiClient mGoogleApiClient;

    private ProgressDialog mProgressDialog;

    final BancoController db = new BancoController(this);

    private int pontos_virtuais_id_pontos_virtuais = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        facebookSDKInitialize();
        setContentView(R.layout.activity_login);
        LoginButton loginButton = (LoginButton) findViewById(R.id.botao_FB);
        loginButton.setReadPermissions("email");
        getLoginDetails(loginButton);

        findViewById(R.id.sign_in_button).setOnClickListener(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .requestEmail()
                .build();

        // ATTENTION: This "addApi(AppIndex.API)"was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(AppIndex.API).build();

        SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.COLOR_LIGHT);
        signInButton.setScopes(gso.getScopeArray());

        if (isLoggedIn()) {
            Intent intent = new Intent(LoginActivity.this, MapsActivity.class);
            startActivity(intent);
            finish();
        }

        //Leitura();
    }

    protected void facebookSDKInitialize() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
    }

    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        Profile profile = Profile.getCurrentProfile();

        return accessToken != null && profile != null;
    }

    protected void getLoginDetails(LoginButton login_button) {

        login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult login_result) {
                getUserInfo(login_result);
            }

            @Override
            public void onCancel() {
                Log.i("Script", "Cancelado");
            }

            @Override
            public void onError(FacebookException e) {
                Log.d("TAG", "onError " + e);
                Toast.makeText(getApplicationContext(), "Falha ao conectar", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();

        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        mGoogleApiClient.connect();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    protected void getUserInfo(LoginResult login_result) {

        GraphRequest data_request = GraphRequest.newMeRequest(login_result.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject json_object, GraphResponse response) {

                        sharedPreferences = getSharedPreferences(DADOS_USUARIO, 0);
                        SharedPreferences.Editor editor = sharedPreferences.edit();

                        final String name = json_object.optString("name");
                        final String email = json_object.optString("email");
                        final String idrs = json_object.optString("id");

                        System.out.println("ATiii RESPOSTA IDRS FB:  " + idrs);

                        editor.putString("nome", name);
                        editor.putString("email", email);
                        editor.putString("idrs", idrs);
                        editor.commit();

                        Leitura();

                        if (db.verificaUsuario() > 0) {
                            db.atualizaUsuario(new Usuario(1, name, email, idrs, pontos_virtuais_id_pontos_virtuais));
                            Atualiza(name, email, idrs);
                            Intent intent = new Intent(LoginActivity.this, AjudaActivity.class);
                            startActivity(intent);
                        } else {
                            db.adicionaUsuario(new Usuario(1, name, email, idrs, pontos_virtuais_id_pontos_virtuais));
                            Intent intent = new Intent(LoginActivity.this, AjudaActivity.class);
                            startActivity(intent);
                        }
                    }
                }
        );

        Bundle permission_param = new Bundle();
        permission_param.putString("fields", "id,name,email,picture.width(100).height(100)");
        data_request.setParameters(permission_param);
        data_request.executeAsync();

    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {

            sharedPreferences = getSharedPreferences(DADOS_USUARIO, 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            GoogleSignInAccount acct = result.getSignInAccount();

            final String name = acct.getDisplayName();
            final String email = acct.getEmail();
            final String id_redeSocial = acct.getId();
            System.out.println("ATiii RESPOSTA IDRS GOOGLE:  " + id_redeSocial);

            String photoURL = String.valueOf(acct.getPhotoUrl());

            editor.putString("nome", name);
            editor.putString("email", email);
            editor.putString("idrs", id_redeSocial);
            editor.commit();

            Leitura();

            if (db.verificaUsuario() > 0) {
                db.atualizaUsuario(new Usuario(1, name, email, photoURL,
                        pontos_virtuais_id_pontos_virtuais));
                Atualiza(name, email, id_redeSocial);
            } else {
                db.adicionaUsuario(new Usuario(name, email, photoURL,
                        pontos_virtuais_id_pontos_virtuais));
            }

            String pegaarquivo = new MapsActivity().getTOKEN_ACESSO();
            sharedPreferences = this.getSharedPreferences(pegaarquivo, 0);

            if (sharedPreferences.contains("name") && sharedPreferences.contains("idRedeSocial")) {
                Intent intent = new Intent(LoginActivity.this, MapsActivity.class);
                startActivity(intent);
            } else {
                Intent intent = new Intent(LoginActivity.this, AjudaActivity.class);
                startActivity(intent);
            }
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
        }
    }

    public void Cadastra(String nome, String email, String id_rede_social) {

        HashMap<String, String> data = new HashMap<>();

        data.put("url", "http://paulo.winserverpro.com.br/API/Usuario.php");
        data.put("acao", "Cadastra");

        data.put("nome", nome);
        data.put("email", email);
        data.put("id_rede_social", id_rede_social);
        data.put("pontos_virtuais", "0");

        ConnectionAPI conexao = new ConnectionAPI();
        conexao.delegate = contexto;
        conexao.execute(data);
//        System.out.println("ATIII DATA CADASTRO LOGIN: " + data);
    }

    public void Atualiza(String nome, String email, String id_rede_social) {

        HashMap<String, String> data = new HashMap<>();

        data.put("url", "http://paulo.winserverpro.com.br/API/Usuario.php");
        data.put("acao", "Atualiza");

        sharedPreferences = getSharedPreferences(DADOS_USUARIO, 0);
        data.put("id", sharedPreferences.getString("id", ""));
        data.put("nome", nome);
        data.put("email", email);
        data.put("id_rede_social", id_rede_social);

        ConnectionAPI conexao = new ConnectionAPI();
        conexao.delegate = contexto;
        conexao.execute(data);
    }

    public void Leitura() {

        HashMap<String, String> data = new HashMap<>();

        sharedPreferences = getSharedPreferences(DADOS_USUARIO, 0);

        data.put("url", "http://paulo.winserverpro.com.br/API/Usuario.php");
        data.put("acao", "Leitura");
        data.put("id_rede_social", sharedPreferences.getString("idrs", ""));

        ConnectionAPI conexao = new ConnectionAPI();
        conexao.delegate = contexto;
        conexao.execute(data);
    }

    @Override
    public void processFinish(JSONObject resposta) {
        try {

            //System.out.println("ATiii RESPOSTA LOGIN:  " + resposta);

            sharedPreferences = getSharedPreferences(DADOS_USUARIO, 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            String status = resposta.getString("status");

            if (status.equals("2")) {
                editor.putString("id", resposta.getString("id"));
                System.out.println("ATiii ARMAZENOU ID:  " + resposta.getString("id"));
                editor.commit();
            }

            if (status.equals("3")) {
                JSONArray respostaArray = resposta.getJSONArray("resposta");
                for (Integer i = 0; i <= respostaArray.length(); i++) {
                    JSONObject respostaObject = respostaArray.getJSONObject(i);
                    editor.putString("id", respostaObject.getString("id"));
                    System.out.println("ATiii ARMAZENOU ID:  " + respostaObject.getString("id"));
                    editor.commit();
                }
            } else {
                Cadastra(sharedPreferences.getString("nome", ""),
                        sharedPreferences.getString("email", ""),
                        sharedPreferences.getString("idrs", ""));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
    }
}

