package br.selectv.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.plus.Plus;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Objects;

import br.selectv.R;
import br.selectv.model.Usuario;
import br.selectv.util.*;
import de.hdodenhof.circleimageview.CircleImageView;
import de.mrapp.android.bottomsheet.BottomSheet;
import io.fabric.sdk.android.Fabric;
import me.drakeet.materialdialog.MaterialDialog;

public class MapsActivity extends AppCompatActivity implements
        LocationListener,
        NavigationView.OnNavigationItemSelectedListener,
        ActivityCompat.OnRequestPermissionsResultCallback,
        GoogleApiClient.OnConnectionFailedListener, AsyncConect, OnMapReadyCallback {

    public static final int REQUEST_PERMISSIONS_CODE = 128;

    int SPLASH_TIME_OUT = 500;

    private MapsActivity contexto = MapsActivity.this;
    private MaterialDialog mMaterialDialog;
    private GoogleMap mMap;
    private LocationManager locationManager;
    private boolean allowNetwork;

    Integer indiceMaker = 0;
    Integer clicado = 0;
    HashMap makerId = new HashMap();

    protected Location mCurrentLocation;

    private final String TOKEN_ACESSO = "token_acesso";
    private final String TOKEN_MEDALHA = "token_medalha";
    private final String PONTO_COLETA = "ponto_coleta";

    private ProgressDialog pD;
    private int medalhaPonto = 0;
    private int medalhaCheck = 0;

    public String getTOKEN_ACESSO() {
        return TOKEN_ACESSO;
    }

    public String getTOKEN_MEDALHA() {
        return TOKEN_MEDALHA;
    }

    TextView user_name, user_email;
    ProfilePictureView profilePictureView;
    CircleImageView profileImage;
    NavigationView navigation_view;
    ShareDialog shareDialog;
    SharedPreferences sharedPreferences,
            sharedPreferences1,
            sharedPreferences2;

    CheckBox cbpapel, cbplastico, cbvidro, cbmetal, cborganico;
    EditText txtDescricao;

    final BancoController db = new BancoController(this);
    private GoogleApiClient mGoogleApiClient;
    private final Context mContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setNavigationHeader();    // call setNavigationHeader Method.

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        assert drawer != null;
        assert navigationView != null;

        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        //VER ISSO AQUI
        navigationView.setCheckedItem(0);

        /*SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mMap = mapFrag.getMap();*/

        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(Plus.API)
                .build();

        shareDialog = new ShareDialog(this);  // intialize facebook shareDialog.

        TwitterAuthConfig authConfig = new TwitterAuthConfig("consumerKey", "consumerSecret");
        Fabric.with(this, new TwitterCore(authConfig), new TweetComposer());

        LeituraCadastro();

        pD = new ProgressDialog(mContext);
        pD.setMessage("Aguardando localização!");
        pD.setIndeterminate(false);
        pD.setCancelable(false);
        pD.show();
    }

    public void setNavigationHeader() {

        navigation_view = (NavigationView) findViewById(R.id.nav_view);

        View header = LayoutInflater.from(this).inflate(R.layout.nav_header_main, null);
        navigation_view.addHeaderView(header);

        user_name = (TextView) header.findViewById(R.id.username);
        user_email = (TextView) header.findViewById(R.id.email);

        profileImage = (CircleImageView) header.findViewById(R.id.profileImage);
        profilePictureView = (ProfilePictureView) header.findViewById(R.id.profile_pic);

        if (isLoggedIn())
            profileImage.setVisibility(View.GONE);
        else
            profilePictureView.setVisibility(View.GONE);

        if (db.verificaUsuario() > 0) {

            Usuario usuario = db.obtemUsuario(1);

            String pegaarquivo = new MapsActivity().getTOKEN_ACESSO();
            sharedPreferences = this.getSharedPreferences(pegaarquivo, 0);
            if (sharedPreferences.contains("name") || sharedPreferences.contains("idRedeSocial")) {
                Global.toastLong(mContext, (String.format(getString(R.string.bem_vindo_novamete), usuario.getNome())));
            } else {
                Global.toastLong(mContext, (String.format(getString(R.string.bem_vindo), usuario.getNome())));
            }

            user_name.setText(usuario.getNome());
            user_email.setText(usuario.getEmail());

            sharedPreferences = getSharedPreferences(TOKEN_ACESSO, 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            editor.putString("name", usuario.getNome());
            editor.putString("idRedeSocial", usuario.getIdrs());

            editor.commit();

            profilePictureView.setProfileId(usuario.getIdrs());

            Picasso.with(mContext)
                    .load(usuario.getIdrs())
                    .placeholder(R.drawable.ic_account_circle)
                    .error(R.drawable.ic_account_circle)
                    .into(profileImage);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSIONS_CODE:
                for (int i = 0; i < permissions.length; i++) {

                    if (permissions[i].equalsIgnoreCase(Manifest.permission.ACCESS_FINE_LOCATION)
                            && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        onResume();
                    }
                }
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void adicionarMarker(View view) {

        if (mCurrentLocation != null) {

            LayoutInflater inflater = getLayoutInflater();

            View alertLayout = inflater.inflate(R.layout.activity_cad_marker, null);

            txtDescricao = (EditText) alertLayout.findViewById(R.id.txDescricao);
            cbpapel = (CheckBox) alertLayout.findViewById(R.id.papel);
            cbplastico = (CheckBox) alertLayout.findViewById(R.id.plastico);
            cbvidro = (CheckBox) alertLayout.findViewById(R.id.vidro);
            cbmetal = (CheckBox) alertLayout.findViewById(R.id.metal);
            cborganico = (CheckBox) alertLayout.findViewById(R.id.organico);

            AlertDialog.Builder mensagemExibir = new AlertDialog.Builder(this);
            mensagemExibir.setTitle("Tipo:");
            mensagemExibir.setCancelable(true);
            mensagemExibir.setView(alertLayout);

            mensagemExibir.setPositiveButton(" Ok ", new DialogInterface.OnClickListener() {

                @SuppressLint("NewApi")
                public void onClick(DialogInterface dialog, int which) {

                    /*if (cadMarker <= 10) {*/

                    final String txtRef = txtDescricao.getText().toString();

                    String papel = "";
                    String plastico = "";
                    String vidro = "";
                    String metal = "";
                    String organico = "";

                    if (cbpapel.isChecked())
                        papel = "Papel <br>";

                    if (cbplastico.isChecked())
                        plastico = "Plástico <br>";

                    if (cbvidro.isChecked())
                        vidro = "Vidro <br>";

                    if (cbmetal.isChecked())
                        metal = "Metal <br>";

                    if (cborganico.isChecked())
                        organico = "Orgânico <br>";

                    if (!cbpapel.isChecked() && !cbplastico.isChecked() && !cbvidro.isChecked() && !cbmetal.isChecked()
                            && !cborganico.isChecked() || Objects.equals(txtRef, "")) {

                        Global.toastLong(mContext, R.string.validacao_pontocoleta);

                    } else {

                            /*LatLng posicao = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());

                            customAddMarker(respostaObject.get("id"), posicao, "Ponto de Coleta", "Tipo de material: <br>" + papel + plastico +
                                    vidro + metal + organico + "<br>Ponto de Referência:<br>" + txtRef, respostaObject.getString("tipo"));

                            customAddMarker(posicao, "Ponto de Coleta", "Tipo de material: <br>" + papel + plastico +
                                    vidro + metal + organico + "<br>Ponto de Referência:<br>" + txtRef);*/

                        System.out.println("ATIII: POP MARCADOR" + "Latitude: " + mCurrentLocation.getLatitude() + "Longitude:"
                                + mCurrentLocation.getLongitude() + "Tipo: " + papel + " "
                                + plastico + " " + vidro + " " + metal + " " + organico
                                + "Ponto de Referencia " + txtRef + " ");

                        sharedPreferences1 = getSharedPreferences(PONTO_COLETA, 0);
                        SharedPreferences.Editor editor = sharedPreferences1.edit();

                        editor.putString("longitude", String.valueOf(mCurrentLocation.getLongitude()));
                        editor.putString("latitude", String.valueOf(mCurrentLocation.getLatitude()));
                        editor.putString("tipo", papel + plastico + vidro + metal + organico);
                        editor.putString("pt_referencia", "<br>Ponto de Referência:<br>" + txtRef);
                        editor.commit();

                        if (Global.isOnline(mContext)) {
                            CadastraPontoColeta();
                            LeituraPontoColeta();
                        } else {
                            Global.toastShort(mContext, R.string.internet_failed);
                        }

                        medalhaPonto = medalhaPonto + 15;
                        AtualizaCadastro("15");
                        LeituraCadastro();

                        sharedPreferences2 = getSharedPreferences(TOKEN_MEDALHA, 0);

                        if (!sharedPreferences2.contains("ponto_coleta")) {
                            addMedalha();
                        }

                        SharedPreferences.Editor editor1 = sharedPreferences2.edit();
                        editor1.putString("ponto_coleta", "");
                        editor1.commit();

                    }

                    dialog.dismiss();
                }
            });

            mensagemExibir.setNegativeButton(" Cancelar ", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    dialog.cancel();
                }
            });

            mensagemExibir.create();
            mensagemExibir.show();

        } else {
            Global.toastShort(mContext, R.string.aguardando_localizacao);
        }

    }

    public void myLocation(View view) {
        Global.myLocation(this, mMap, mCurrentLocation);

    }

    public void customAddMarker(final String id, LatLng latLng, String title, String snippet, final String tipo) {
        makerId.put("m" + indiceMaker, id);
        indiceMaker++;
        MarkerOptions options = new MarkerOptions();
        options.position(latLng).title(title).snippet(snippet);

        if (tipo.contains("Papel <br>Plástico <br>") || tipo.contains("Papel <br>Vidro <br>")
                || tipo.contains("Papel <br>Metal <br>") || tipo.contains("Papel <br>Orgânico <br>")
                || tipo.contains("Plástico <br>Vidro <br>") || tipo.contains("Plástico <br>Metal <br>")
                || tipo.contains("Plástico <br>Orgânico <br>") || tipo.contains("Vidro <br>Metal <br>")
                || tipo.contains("Vidro <br>Orgânico <br>") || tipo.contains("Metal <br>Orgânico <br>")) {
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.todos));
            mMap.addMarker(options);

        } else if (tipo.equals("Papel <br>")) {
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.papel));
            mMap.addMarker(options);

        } else if (tipo.equals("Plástico <br>")) {
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.plastico));
            mMap.addMarker(options);

        } else if (tipo.equals("Vidro <br>")) {
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.vidro));
            mMap.addMarker(options);

        } else if (tipo.equals("Metal <br>")) {
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.metal));
            mMap.addMarker(options);

        } else if (tipo.equals("Orgânico <br>")) {
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.organico));
            mMap.addMarker(options);

        }

        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                clicado = Integer.parseInt((String) makerId.get(marker.getId()));

                LinearLayout li = new LinearLayout(MapsActivity.this);
                TextView tv = new TextView(MapsActivity.this);
                tv.setText(Html.fromHtml("<b><center>" + marker.getTitle()
                        + "</b></center><br><br>" + marker.getSnippet()));
                tv.setTextColor(Color.BLACK);
                tv.setTextSize(12);

                li.addView(tv);

                ImageView im = new ImageView(MapsActivity.this);
                im.setBackgroundResource(R.drawable.ic_information_circled);
                li.addView(im);

                return li;
            }
        });

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

            @Override
            public void onInfoWindowClick(Marker marker) {
                ComentarCompartilhar();

            }
        });

    }

    public void addMedalha() {

        if (medalhaPonto == 15 || medalhaCheck == 11) {

            Snackbar snackbar = Snackbar
                    .make(findViewById(android.R.id.content), R.string.recompensaMedalha, Snackbar.LENGTH_LONG)
                    .setAction("Ver", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(MapsActivity.this, MedalhaActivity.class);
                            startActivity(intent);
                        }
                    });
            snackbar.show();

        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (Global.isOnline(mContext)) {
            LeituraPontoColeta();
        } else {
            Global.toastShort(mContext, R.string.internet_failed);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                callDialog(R.string.permissaoOK, new String[]{Manifest.permission.ACCESS_FINE_LOCATION});
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSIONS_CODE);
            }
        } else {

            allowNetwork = true;
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);
                builder.setCancelable(true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder.setView(R.layout.activity_pop_up);
                }

                builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Intent it = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(it);
                        dialog.dismiss();
                    }
                });

                builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        dialog.cancel();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();

            } else {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }

                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 60 * 60 * 1000, 1000, this);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.removeUpdates(this);

    }

    public void configLocation(LatLng latLng) {

        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(17).build();
        CameraUpdate update = CameraUpdateFactory.newCameraPosition(cameraPosition);
        pD.dismiss();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mMap.setMyLocationEnabled(true);
        mMap.animateCamera(update);
    }

    public void ComentarCompartilhar() {
        BottomSheet.Builder builder = new BottomSheet.Builder(MapsActivity.this);
        builder.setTitle("Escolha uma opção");
        final int Comentar = 0;
        builder.addItem(Comentar, R.string.comentar, R.drawable.ic_comment);
        int compartilhar = 1;
        builder.addItem(compartilhar, R.string.compartilhar, R.drawable.ic_check_in);
        BottomSheet bottomSheet = builder.create();
        bottomSheet.setStyle(BottomSheet.Style.GRID);
        bottomSheet.show();
        bottomSheet.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (id == Comentar) {
                    if (Global.isOnline(mContext)) {
                        eventoComent(view);
                    } else {
                        Global.toastLong(mContext, R.string.internet_failed);
                    }
                } else {
                    if (Global.isOnline(mContext)) {
                        eventoCheck(view);
                    } else {
                        Global.toastLong(mContext, R.string.internet_failed);
                    }
                }

            }
        });
    }

    public void eventoComent(View view) {
        System.out.println("ATiii ID AQUI clicado : " + clicado);
        Intent intent = new Intent();
        Bundle params = new Bundle();
        String pt_coleta = clicado.toString();
        params.putString("id_ponto_coleta", pt_coleta);
        intent.putExtras(params);
        intent.setClass(this, ComentarioActivity.class);
        startActivity(intent);
    }

    public void eventoCheck(View view) {
        sharedPreferences2 = getSharedPreferences(TOKEN_MEDALHA, 0);
        Global.toastLong(mContext, R.string.checkin_ok);

        medalhaCheck = medalhaCheck + 11;
        AtualizaCadastro("11");
        LeituraCadastro();

        if (!sharedPreferences2.contains("check")) {
            addMedalha();
        }

        SharedPreferences.Editor editor1 = sharedPreferences2.edit();
        editor1.putString("check", "");
        editor1.commit();


        new Handler().postDelayed(new Runnable() {

        /*
        * Exibindo splash com um timer.
         */

            @Override
            public void run() {

                AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);
                builder.setCancelable(true)
                        .setTitle("Deseja compatilhar seu check-in?")
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {

                                BottomSheet.Builder builder = new BottomSheet.Builder(MapsActivity.this);
                                builder.setTitle("Escolha uma opção");
                                final int Facebook = 0;
                                builder.addItem(Facebook, R.string.facebook, R.drawable.facebook);
                                int Twitter = 1;
                                builder.addItem(Twitter, R.string.twitter, R.drawable.twitter);
                                BottomSheet bottomSheet = builder.create();
                                bottomSheet.setStyle(BottomSheet.Style.GRID);
                                bottomSheet.show();

                                bottomSheet.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                        if (id == Facebook) {
                                            if (Global.isOnline(mContext)) {
                                                shareFB(view);
                                            } else {
                                                Global.toastLong(mContext, R.string.internet_failed);
                                            }
                                        } else {
                                            if (Global.isOnline(mContext)) {
                                                tweetTT(view);
                                            } else {
                                                Global.toastLong(mContext, R.string.internet_failed);
                                            }
                                        }

                                    }
                                });

                            }
                        });

                builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        dialog.cancel();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }, SPLASH_TIME_OUT);

    }

    public void tweetTT(View view) {
        Usuario usuario = db.obtemUsuario(1);

        /*File myImageFile = new File("/path/to/image");
        Uri myImageUri = Uri.fromFile(myImageFile);*/
        TweetComposer.Builder builder = null;
        try {
            builder = new TweetComposer.Builder(this)
                    .text((usuario.getNome()) + " efetuou um descarte de resíduo doméstico no aplicativo SelectV.\n")
                    .url(new URL("https://play.google.com/store/apps/details?id=br.selectv"));
            //.image(myImageUri);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        builder.show();

    }

    public void shareFB(View view) {
        Usuario usuario = db.obtemUsuario(1);

        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                .setContentTitle((usuario.getNome()) + " efetuou um descarte de resíduo doméstico")
                .setImageUrl(Uri.parse("https://scontent-iad3-1.xx.fbcdn.net/v/t1.0-0/p403x403/14040005_1189692941082203_5314054286389019409_n.jpg?oh=72d28bdba175fdde77144db8abf8ca00&oe=584DE06D"))
                .setContentDescription("no aplicativo SelectV.")
                .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=br.selectv"))
                .build();

        shareDialog.show(linkContent);  // Show facebook ShareDialog
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;

        if (location != null) {
            if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
                allowNetwork = false;
            }

            if (allowNetwork || location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
                configLocation(new LatLng(location.getLatitude(), location.getLongitude()));
            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    // UTIL
    private void callDialog(int message, final String[] permissions) {
        mMaterialDialog = new MaterialDialog(this)
                .setTitle("Permission")
                .setMessage(message)
                .setPositiveButton("Ok", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ActivityCompat.requestPermissions(MapsActivity.this, permissions, REQUEST_PERMISSIONS_CODE);
                        mMaterialDialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                    }
                });
        mMaterialDialog.show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.mapa) {
            onResume();

        } else if (id == R.id.ranking) {
//            Global.toastShort(mContext, "Disponível em um atualização futura!");

            Intent intent = new Intent(MapsActivity.this, RankingActivity.class);
            startActivity(intent);
        } else if (id == R.id.medalhas) {

            //Global.toastShort(mContext, "Disponível em um atualização futura!");
            Intent intent = new Intent(MapsActivity.this, MedalhaActivity.class);
            startActivity(intent);

        } else if (id == R.id.ajuda) {

            Intent intent = new Intent(MapsActivity.this, AjudaActivity.class);
            startActivity(intent);

        } else if (id == R.id.sobre) {

            Intent intent = new Intent(MapsActivity.this, SobreActivity.class);
            startActivity(intent);

        } else if (id == R.id.log_out) {
            if (isLoggedIn()) {
                LoginManager.getInstance().logOut();
            } else {
                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                        new ResultCallback<Status>() {
                            @Override
                            public void onResult(@NonNull Status status) {
                            }
                        });
            }

            sharedPreferences = getSharedPreferences(TOKEN_ACESSO, 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.commit();

            sharedPreferences1 = getSharedPreferences(PONTO_COLETA, 0);
            SharedPreferences.Editor editor1 = sharedPreferences1.edit();
            editor1.clear();
            editor1.commit();

            Intent intent = new Intent(MapsActivity.this, LoginActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        Profile profile = Profile.getCurrentProfile();

        return accessToken != null && profile != null;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void CadastraPontoColeta() {

        HashMap<String, String> data = new HashMap<>();

        data.put("url", "http://paulo.winserverpro.com.br/API/PontoColeta.php");
        data.put("acao", "Cadastra");

        String pegaarquivo = new LoginActivity().getDADOS_USUARIO();
        sharedPreferences = this.getSharedPreferences(pegaarquivo, 0);

        sharedPreferences1 = getSharedPreferences(PONTO_COLETA, 0);

        data.put("id_usuario", sharedPreferences.getString("id", ""));
        data.put("pt_referencia", sharedPreferences1.getString("pt_referencia", ""));
        data.put("tipo", sharedPreferences1.getString("tipo", ""));
        data.put("latitude", sharedPreferences1.getString("latitude", ""));
        data.put("longitude", sharedPreferences1.getString("longitude", ""));

        ConnectionAPI conexao = new ConnectionAPI();
        conexao.delegate = contexto;
        conexao.execute(data);
    }

    public void AtualizaCadastro(String pt_virtuais) {

        String pegaarquivo = new LoginActivity().getDADOS_USUARIO();
        sharedPreferences = this.getSharedPreferences(pegaarquivo, 0);

        sharedPreferences2 = getSharedPreferences(TOKEN_MEDALHA, 0);

        Usuario usuario = db.obtemUsuario(1);

        HashMap<String, String> data = new HashMap<>();

        data.put("url", "http://paulo.winserverpro.com.br/API/Usuario.php");
        data.put("acao", "Atualiza");

        data.put("id", sharedPreferences.getString("id", ""));
        data.put("nome", usuario.getNome());
        data.put("email", usuario.getEmail());
        data.put("pontos_virtuais", String.valueOf(Integer.parseInt(sharedPreferences2.getString("pontos_virtuais",""))
                + Integer.parseInt(pt_virtuais)));
        data.put("id_rede_social", sharedPreferences.getString("idrs", ""));

        ConnectionAPI conexao = new ConnectionAPI();
        conexao.delegate = contexto;
        conexao.execute(data);
    }

    public void LeituraCadastro() {
        String pegaarquivo = new LoginActivity().getDADOS_USUARIO();
        sharedPreferences = this.getSharedPreferences(pegaarquivo, 0);

        HashMap<String, String> data = new HashMap<>();

        data.put("url", "http://paulo.winserverpro.com.br/API/Usuario.php");
        data.put("acao", "Leitura");

        data.put("id", sharedPreferences.getString("id", ""));

        ConnectionAPI conexao = new ConnectionAPI();
        conexao.delegate = contexto;
        conexao.execute(data);
    }

    public void LeituraPontoColeta() {

        HashMap<String, String> data = new HashMap<>();

        data.put("url", "http://paulo.winserverpro.com.br/API/PontoColeta.php");
        data.put("acao", "Leitura");

        data.put("id", "");

        ConnectionAPI conexao = new ConnectionAPI();
        conexao.delegate = contexto;
        conexao.execute(data);
    }

    @Override
    public void processFinish(JSONObject resposta) {

        System.out.println("ATiiii RESPOSTA MAPS" + resposta);

try {
            Global.toastShort(mContext, resposta.getString("mensagem"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONArray respostaLogin = resposta.getJSONArray("resposta");
            for (Integer a = 0; a <= respostaLogin.length(); a++) {
                JSONObject respostaObjectLogin = respostaLogin.getJSONObject(a);

                sharedPreferences2 = getSharedPreferences(TOKEN_MEDALHA, 0);
                SharedPreferences.Editor editor1 = sharedPreferences2.edit();
                editor1.putString("pontos_virtuais", respostaObjectLogin.getString("pontos_virtuais"));
                editor1.commit();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {

            JSONArray respostaArray = resposta.getJSONArray("resposta");
            for (Integer i = 0; i <= respostaArray.length(); i++) {
                JSONObject respostaObject = respostaArray.getJSONObject(i);
/*                System.out.println("ATiiii id: " + respostaObject.getString("id"));
                System.out.println("ATiiii pt: " + respostaObject.getString("pt_referencia"));
                System.out.println("ATiiii tp: " + respostaObject.getString("tipo"));
                System.out.println("ATiiii lat: " + respostaObject.getString("latitude"));
                System.out.println("ATiiii log: " + respostaObject.getString("longitude"));*/

                double lat = Double.parseDouble(String.valueOf(respostaObject.get("latitude")));
                double log = Double.parseDouble(String.valueOf(respostaObject.get("longitude")));

                LatLng posicao = new LatLng(lat, log);

                customAddMarker(respostaObject.getString("id"), posicao, "Ponto de Coleta", "Tipo de material: <br>" + (respostaObject.getString("tipo"))
                        + (respostaObject.getString("pt_referencia")), respostaObject.getString("tipo"));

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMyLocationButtonEnabled(false);

    }

}

