package br.selectv.activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import br.selectv.R;

public class SobreActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sobre);

        TextView tvVersion = (TextView) findViewById(R.id.txtVersion);
        Button btShare = (Button) findViewById(R.id.button_compartilhar);
        Button btRate = (Button) findViewById(R.id.button_avaliar);

        assert tvVersion != null;
        assert btShare != null;
        assert btRate != null;

        PackageInfo pInfo = null;
        try {
            pInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        tvVersion.setText(getString(R.string.txt_versão, pInfo.versionName));

        btShare.setOnClickListener(this);
        btRate.setOnClickListener(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();

        }
        return true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            //ALETRAR O PLAY LINK QUANDO APLICATIVO FOR PUBLICADO

            case R.id.button_compartilhar:
                String subject = getString(R.string.share_subject);
                String text = getString(R.string.share_text) + "\n\n" + getString(R.string.play_link);
                String sendWith = getString(R.string.share_label);
                shareText(subject, text, sendWith);
                break;

            case R.id.button_avaliar:
                Uri uri = Uri.parse(getString(R.string.play_link));
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;
        }

    }

    public void shareText(String subject, String text, String sendWith) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, text);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        startActivity(Intent.createChooser(intent, sendWith));
    }

    @Override
    public void onBackPressed() {
    }

}
