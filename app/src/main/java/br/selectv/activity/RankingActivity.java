package br.selectv.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import br.selectv.R;
import br.selectv.model.RankingListView;
import br.selectv.util.AdapterListViewRanking;
import br.selectv.util.AsyncConect;
import br.selectv.util.ConnectionAPI;
import br.selectv.util.Global;

public class RankingActivity extends AppCompatActivity implements AsyncConect {

    private RankingActivity contexto = RankingActivity.this;

    private AdapterListViewRanking adapterListView;
    private ArrayList<RankingListView> ListRanking;

    private final Context mContext = this;

    private ListView listRanking;

    ProgressDialog pD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listRanking = (ListView) findViewById(R.id.listViewRanking);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (Global.isOnline(mContext)) {
            LeituraRanking();
        } else {
            Global.toastShort(mContext, R.string.internet_failed);
        }
    }

    private void LeituraRanking() {

        HashMap<String, String> data = new HashMap<>();

        data.put("url", "http://paulo.winserverpro.com.br/API/Usuario.php");
        data.put("acao", "Leitura");
        data.put("pontos_virtuais", "1");

        ConnectionAPI conexao = new ConnectionAPI();
        conexao.delegate = contexto;
        conexao.execute(data);

        pD = new ProgressDialog(mContext);
        pD.setMessage("Carregando informações");
        pD.setIndeterminate(false);
        pD.setCancelable(false);
        pD.show();
    }

    @Override
    public void processFinish(JSONObject resposta) {
        pD.dismiss();
        try {
            ListRanking = new ArrayList<>();

            System.out.println(resposta);

            JSONArray respostaAPI = resposta.getJSONArray("resposta");
            for (int i = 0; i < respostaAPI.length(); i++) {
                JSONObject resultadoAPI = respostaAPI.getJSONObject(i);
                String nomeAPI = resultadoAPI.getString("nome");
                String ptVirtuaisAPI = resultadoAPI.getString("pontos_virtuais");

                if (ptVirtuaisAPI.equals("null")) {
                    ptVirtuaisAPI = "0";
                }

                RankingListView listaRanking = new RankingListView(nomeAPI, ptVirtuaisAPI);

                ListRanking.add(listaRanking);

                adapterListView = new AdapterListViewRanking(this, ListRanking);

                listRanking.setAdapter(adapterListView);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();

        }
        return true;
    }

    @Override
    public void onBackPressed() {
    }

}
