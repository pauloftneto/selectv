package br.selectv.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Network;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.RatingBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import br.selectv.R;
import br.selectv.model.ComentariosListView;
import br.selectv.model.Usuario;
import br.selectv.util.AdapterListViewComentario;
import br.selectv.util.AsyncConect;
import br.selectv.util.BancoController;
import br.selectv.util.ConnectionAPI;
import br.selectv.util.Global;
import br.selectv.util.Input;

public class ComentarioActivity extends AppCompatActivity implements AsyncConect {

    private SharedPreferences sharedPreferences,
            sharedPreferences2;
    private ComentarioActivity contexto = ComentarioActivity.this;

    private AdapterListViewComentario adapterListView;
    private ArrayList<ComentariosListView> ListComentarios;

    final BancoController db = new BancoController(this);

    private ListView listComentario;

    String mostraTexto;
    AutoCompleteTextView txComentario;
    RatingBar mRatingNote;

    private final Context mContext = this;

    ProgressDialog pD;
    private int medalhaComent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comentario);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txComentario = (AutoCompleteTextView) findViewById(R.id.txComentario);
        //txAvaliacao = (AutoCompleteTextView) findViewById(R.id.txAvaliacao);
        mRatingNote = (RatingBar) findViewById(R.id.rating_note);

        listComentario = (ListView) findViewById(R.id.listComentario);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle params = getIntent().getExtras();
        if (params != null) {
            mostraTexto = params.getString("id_ponto_coleta");
            System.out.println("ATiii ID AQUI clicado coment " + mostraTexto);
        }

        LeituraComentario();

    }

    public void LeituraComentario() {

        HashMap<String, String> data = new HashMap<>();

        data.put("url", "http://paulo.winserverpro.com.br/API/Comentario.php");
        data.put("acao", "Leitura");
        data.put("id_ponto_coleta", mostraTexto);

        ConnectionAPI conexao = new ConnectionAPI();
        conexao.delegate = contexto;
        conexao.execute(data);

        pD = new ProgressDialog(mContext);
        pD.setMessage("Carregando informações");
        pD.setIndeterminate(false);
        pD.setCancelable(false);
        pD.show();
    }

    public void CadastraComentario() {
        float ratingNote = Input.value(mRatingNote);
        if (ratingNote < 1) {
            Snackbar.make(findViewById(android.R.id.content), R.string.validate_rating, Snackbar.LENGTH_LONG).show();
        } else {
            HashMap<String, String> data = new HashMap<>();

            data.put("url", "http://paulo.winserverpro.com.br/API/Comentario.php");
            data.put("acao", "Cadastra");

            String pegaarquivo = new LoginActivity().getDADOS_USUARIO();
            sharedPreferences = this.getSharedPreferences(pegaarquivo, 0);

            data.put("id_usuario", sharedPreferences.getString("id", ""));
            data.put("id_ponto_coleta", mostraTexto);
            data.put("descricao", txComentario.getText().toString());
            data.put("avaliacao", String.valueOf(ratingNote));

            ConnectionAPI conexao = new ConnectionAPI();
            conexao.delegate = contexto;
            conexao.execute(data);
        }
    }

    @Override
    public void processFinish(JSONObject resposta) {
        pD.dismiss();
        try {
            ListComentarios = new ArrayList<>();

            System.out.println("ATIII RESPOSTA COMENTARIO" + resposta);

            JSONArray respostaAPI = resposta.getJSONArray("resposta");
            for (int i = 0; i < respostaAPI.length(); i++) {
                JSONObject resultadoAPI = respostaAPI.getJSONObject(i);
                String descricaoAPI = resultadoAPI.getString("descricao");
                String avaliacaoAPI = resultadoAPI.getString("avaliacao");

                if (avaliacaoAPI.equals("null")) {
                    avaliacaoAPI = "";
                }

                ComentariosListView listaCategoria = new ComentariosListView(descricaoAPI, avaliacaoAPI);

                ListComentarios.add(listaCategoria);

                adapterListView = new AdapterListViewComentario(this, ListComentarios);

                listComentario.setAdapter(adapterListView);

            }

            Global.toastShort(mContext, resposta.getString("mensagem"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void buttomComentario(View view) {
        CadastraComentario();
        LeituraComentario();
        txComentario.setText("");

        medalhaComent = medalhaComent + 13;
        AtualizaCadastro(String.valueOf("13"));

        String pegaarquivo = new MapsActivity().getTOKEN_MEDALHA();
        sharedPreferences2 = this.getSharedPreferences(pegaarquivo, 0);

        if (!sharedPreferences2.contains("comentario")) {
            addMedalha();
        }

        SharedPreferences.Editor editor1 = sharedPreferences2.edit();
        editor1.putString("comentario", "");
        editor1.commit();
    }

    public void AtualizaCadastro(String pt_virtuais) {

        String pegaarquivo = new LoginActivity().getDADOS_USUARIO();
        sharedPreferences = this.getSharedPreferences(pegaarquivo, 0);

        String pegaarquivo2 = new MapsActivity().getTOKEN_MEDALHA();
        sharedPreferences2 = getSharedPreferences(pegaarquivo2, 0);

        Usuario usuario = db.obtemUsuario(1);

        HashMap<String, String> data = new HashMap<>();

        data.put("url", "http://paulo.winserverpro.com.br/API/Usuario.php");
        data.put("acao", "Atualiza");

        data.put("id", sharedPreferences.getString("id", ""));
        data.put("nome", usuario.getNome());
        data.put("email", usuario.getEmail());
        data.put("pontos_virtuais", String.valueOf(Integer.parseInt(sharedPreferences2.getString("pontos_virtuais", ""))
                + Integer.parseInt(pt_virtuais)));
        data.put("id_rede_social", sharedPreferences.getString("idrs", ""));

        ConnectionAPI conexao = new ConnectionAPI();
        conexao.delegate = contexto;
        conexao.execute(data);
    }

    public void addMedalha() {

        if (medalhaComent == 13) {

            Snackbar snackbar = Snackbar
                    .make(findViewById(android.R.id.content), R.string.recompensaMedalha, Snackbar.LENGTH_LONG)
                    .setAction("Ver", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(ComentarioActivity.this, MedalhaActivity.class);
                            startActivity(intent);
                        }
                    });
            snackbar.show();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();

        }
        return true;
    }

    @Override
    public void onBackPressed() {

    }

}
