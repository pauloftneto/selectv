package br.selectv.model;

public class Usuario {

    int id_usuario;
    String nome;
    String email;
    String idrs;
    int ptvirtuais;

    public Usuario(int id_usuario, String nome, String email, String idrs, int ptvirtuais) {
        this.id_usuario = id_usuario;
        this.nome = nome;
        this.email = email;
        this.idrs = idrs;
        this.ptvirtuais = ptvirtuais;
    }

    public Usuario(String nome, String email, String idrs, int ptvirtuais) {
        this.nome = nome;
        this.email = email;
        this.idrs = idrs;
        this.ptvirtuais = ptvirtuais;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdrs() {
        return idrs;
    }

    public void setIdrs(String idrs) {
        this.idrs = idrs;
    }

    public int getPtvirtuais() {
        return ptvirtuais;
    }

    public void setPtvirtuais(int ptvirtuais) {
        this.ptvirtuais = ptvirtuais;
    }
}
