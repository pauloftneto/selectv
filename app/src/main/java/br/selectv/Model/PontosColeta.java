
package br.selectv.model;

import java.util.List;

/**
 *
 * @author Paulo Neto
 */


public class PontosColeta {

    int id_pontos_coleta;
    String descricao;
    String tipo;
    List coordenadas;
    String observacoes;
    int valor;
    int nota;
    int id_usuario;

    public int getId_pontos_coleta() {
        return id_pontos_coleta;
    }

    public void setId_pontos_coleta(int id_pontos_coleta) {
        this.id_pontos_coleta = id_pontos_coleta;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public List getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(List coordenadas) {
        this.coordenadas = coordenadas;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

}
