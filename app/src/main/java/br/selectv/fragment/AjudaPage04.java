package br.selectv.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.selectv.R;

public class AjudaPage04 extends Fragment {

    public static AjudaPage04 newInstance() {
        return new AjudaPage04();
    }

    public AjudaPage04() {
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.activity_ajuda_page04, container, false);

    }
}
