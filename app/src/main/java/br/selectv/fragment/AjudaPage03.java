package br.selectv.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.selectv.R;

public class AjudaPage03 extends Fragment {

    public static AjudaPage03 newInstance() {
        return new AjudaPage03();
    }

    public AjudaPage03() {
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.activity_ajuda_page03, container, false);

    }
}
