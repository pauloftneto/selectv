package br.selectv.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.selectv.R;

public class AjudaPage02 extends Fragment {

    public static AjudaPage02 newInstance() {
        return new AjudaPage02();
    }

    public AjudaPage02() {
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.activity_ajuda_page02, container, false);

    }
}
