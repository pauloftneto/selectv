
package br.selectv.model;

/**
 * @author Paulo Neto
 */
public class ComentariosListView {

    private String txDescComentario;
    private String txAvaliacao;

    public ComentariosListView() {
    }

    public ComentariosListView(String txDescComentario, String txAvaliacao) {
        this.txDescComentario = txDescComentario;
        this.txAvaliacao = txAvaliacao;
    }

    public String getTxDescComentario() {
        return txDescComentario;
    }

    public void setTxDescComentario(String txDescComentario) {
        this.txDescComentario = txDescComentario;
    }

    public String getTxAvaliacao() {
        return txAvaliacao;
    }

    public void setTxAvaliacao(String txAvaliacao) {
        this.txAvaliacao = txAvaliacao;
    }
}
