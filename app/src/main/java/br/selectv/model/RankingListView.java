package br.selectv.model;

/**
 * Created by Paulo Neto on 29/08/2016.
 */
public class RankingListView {

    private String txNomeRanking;
    private String txPtVirtuais;

    public RankingListView(String txNomeRanking, String txPtVirtuais) {
        this.txNomeRanking = txNomeRanking;
        this.txPtVirtuais = txPtVirtuais;
    }

    public String getTxNomeRanking() {
        return txNomeRanking;
    }

    public void setTxNomeRanking(String txNomeRanking) {
        this.txNomeRanking = txNomeRanking;
    }

    public String getTxPtVirtuais() {
        return txPtVirtuais;
    }

    public void setTxPtVirtuais(String txPtVirtuais) {
        this.txPtVirtuais = txPtVirtuais;
    }
}
