package br.selectv.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import br.selectv.R;
import br.selectv.model.ComentariosListView;
import br.selectv.model.RankingListView;

/**
 * Created by Paulo Neto on 29/08/2016.
 */
public class AdapterListViewRanking extends BaseAdapter {

    private LayoutInflater mInflater;
    private ArrayList<RankingListView> itens;

    public AdapterListViewRanking(Context context, ArrayList<RankingListView> itens) {
        //Itens que preencheram o listview
        this.itens = itens;
        //responsavel por pegar o Layout do item.
        mInflater = LayoutInflater.from(context);
    }

    /**
     * Retorna a quantidade de itens
     *
     * @return
     */
    public int getCount() {
        return itens.size();
    }

    /**
     * Retorna o item de acordo com a posicao dele na tela.
     *
     * @param position
     * @return
     */
    public RankingListView getItem(int position) {
        return itens.get(position);
    }

    /**
     * Sem implementação
     *
     * @param position
     * @return
     */
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View view, ViewGroup parent) {
        //Pega o item de acordo com a posção.
        RankingListView item = itens.get(position);
        //infla o layout para podermos preencher os dados
        view = mInflater.inflate(R.layout.listview_ranking, null);

        //atravez do layout pego pelo LayoutInflater, pegamos cada id relacionado
        //ao item e definimos as informações.
        ((TextView) view.findViewById(R.id.txNomeRankingAdapter)).setText(item.getTxNomeRanking());
        ((TextView) view.findViewById(R.id.txPtVirtuaisAdapter)).setText(item.getTxPtVirtuais());

        return view;
    }
}

