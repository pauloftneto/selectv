package br.selectv.util;

import org.json.JSONObject;

/**
 * Created by Paulo Neto on 02/08/2016.
 */
public interface AsyncConect {
    void processFinish(JSONObject resposta);
}