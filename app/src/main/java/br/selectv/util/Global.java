package br.selectv.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.net.ConnectivityManager;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import br.selectv.R;

/**
 * Created by Paulo Neto on 10/08/2016.
 */
public class Global {

    private static ProgressDialog pD;

    public static void toastShort(Context context, String menssage) {
        Toast.makeText(context, menssage, Toast.LENGTH_SHORT).show();
    }

    public static void toastShort(Context context, int menssage) {
        Toast.makeText(context, menssage, Toast.LENGTH_SHORT).show();
    }

    public static void toastLong(Context context, String menssage) {
        Toast.makeText(context, menssage, Toast.LENGTH_LONG).show();
    }

    public static void toastLong(Context context, int menssage) {
        Toast.makeText(context, menssage, Toast.LENGTH_LONG).show();
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return manager.getActiveNetworkInfo() != null &&
                manager.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    public static void myLocation(final Activity activity, final GoogleMap map, final Location location) {
        if (location == null) {
            if (activity != null) {
                toastShort(activity, R.string.no_location_yet);
            }
        } else {
            if (map != null && movementNeeded(map, location)) {
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
            }
        }

    }

    private static boolean movementNeeded(GoogleMap googleMap, Location location) {
        LatLng newTarget = new LatLng(location.getLatitude(), location.getLongitude());
        return movementNeeded(googleMap, newTarget);
    }

    private static boolean movementNeeded(GoogleMap googleMap, LatLng newTarget) {
        LatLng cameraTarget = googleMap.getCameraPosition().target;

        return distance(cameraTarget, newTarget) > 1;
    }

    public static double distance(LatLng pos1, LatLng pos2) {
        if (pos1 == null || pos2 == null) return -1;

        return distance(pos1.latitude, pos1.longitude, pos2.latitude, pos2.longitude);
    }

    public static double distance(double lat1, double lon1, double lat2, double lon2) {
        Location location1 = new Location("Location One");
        location1.setLatitude(lat1);
        location1.setLongitude(lon1);

        Location location2 = new Location("Location Two");
        location2.setLatitude(lat2);
        location2.setLongitude(lon2);

        return location1.distanceTo(location2);
    }


}
