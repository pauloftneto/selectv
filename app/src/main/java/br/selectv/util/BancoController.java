package br.selectv.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import br.selectv.model.Usuario;

public class BancoController {

    private DatabaseHelper banco;

    public BancoController(Context context) {
        banco = new DatabaseHelper(context);

    }

    public void adicionaUsuario(Usuario usuario) {

        SQLiteDatabase db = banco.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(DatabaseHelper.NOME, usuario.getNome());
        values.put(DatabaseHelper.EMAIL, usuario.getEmail());
        values.put(DatabaseHelper.IDRS, usuario.getIdrs());
        values.put(DatabaseHelper.QTD_PNT, usuario.getPtvirtuais());

        db.insert(DatabaseHelper.TABELA_USUARIO, null, values);

        db.close();
    }

    public void atualizaUsuario(Usuario usuario) {

        ContentValues values = new ContentValues();
        SQLiteDatabase db = banco.getWritableDatabase();

        values.put(DatabaseHelper.NOME, usuario.getNome());
        values.put(DatabaseHelper.EMAIL, usuario.getEmail());
        values.put(DatabaseHelper.IDRS, usuario.getIdrs());
        values.put(DatabaseHelper.QTD_PNT, usuario.getPtvirtuais());

        db.update(DatabaseHelper.TABELA_USUARIO, values, DatabaseHelper.ID + " = ?",
                new String[] { String.valueOf(usuario.getId_usuario()) });
    }

    public Usuario obtemUsuario(int id) {

        SQLiteDatabase db = banco.getWritableDatabase();

        Cursor cursor = db.query(DatabaseHelper.TABELA_USUARIO, new String[] {DatabaseHelper.ID,
                        DatabaseHelper.NOME, DatabaseHelper.EMAIL, DatabaseHelper.IDRS, DatabaseHelper.QTD_PNT}, DatabaseHelper.ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Usuario usuario = new Usuario(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getInt(4));

        return usuario;
    }

    public int verificaUsuario() {
        String countQuery = "SELECT * FROM " + DatabaseHelper.TABELA_USUARIO;
        SQLiteDatabase db = banco.getWritableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();

        return cnt;
    }

}
