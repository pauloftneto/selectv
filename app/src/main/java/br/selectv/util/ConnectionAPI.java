package br.selectv.util;

import android.os.AsyncTask;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Paulo Neto on 02/08/2016.
 */
public class ConnectionAPI extends  AsyncTask<HashMap<String, String>, Void, JSONObject> {
    public AsyncConect delegate = null;

    private JSONObject retorno = null;

    @Override
    protected JSONObject doInBackground(HashMap<String, String> ... params) {
        try {
            Map<String, String> data = params[0];
            String json = HttpRequest.post(data.get("url")).form(data).body();
            retorno = new JSONObject(json);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retorno;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        delegate.processFinish(jsonObject);
    }
}