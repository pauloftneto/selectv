package br.selectv.util;

import android.text.TextUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RatingBar;

/**
 * Created by Paulo Neto on 02/09/2016.
 */
public class Input {

    public static String value(EditText view) {
        return view.getText().toString();
    }

    public static float value(RatingBar view) {
        return view.getRating();
    }

    public static boolean checked(CheckBox view) {
        return view.isChecked();
    }

    public static int checkedNumber(CheckBox view) {
        return checked(view) ? 1 : 0;
    }

    public static String numbers(EditText view) {
        String value = value(view);
        return value.replaceAll("\\D+", "");
    }

    public static long numbersLong(EditText view) {
        return Long.parseLong(numbers(view));
    }

    public static void requestFocus(EditText view, String text) {
        view.setText("");

        if (!TextUtils.isEmpty(text)) {
            view.append(text);
        }

        view.requestFocus();
    }
}
