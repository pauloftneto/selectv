package br.selectv.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static int VERSAO_BANCO = 1;

    public static final String BANCO_DADOS = "selectv";

    protected static final String TABELA_USUARIO = "usuario";

    protected static final String ID = "id_usuario";
    protected static final String NOME = "nome";
    protected static final String EMAIL = "email";
    protected static final String IDRS = "idrs";
    protected static final String QTD_PNT = "ptvirtuais";

    public DatabaseHelper(Context context) {
        super(context, BANCO_DADOS, null, VERSAO_BANCO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sql = "CREATE TABLE " + TABELA_USUARIO + "("
                + ID + " INTEGER PRIMARY KEY, "
                + NOME + " TEXT NOT NULL, "
                + EMAIL + " TEXT NOT NULL, "
                + IDRS + " INT, "
                + QTD_PNT + " INT" + ")";

        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + TABELA_USUARIO);
        onCreate(db);
    }
}

